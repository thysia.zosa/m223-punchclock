# PunchClockÜberarbeitung
Bearbeiter: Jonas Tochtermann (ionus@gmx.ch)

## Worum geht's?
* Eine kleine App zur Zeiterfassung

## Voraussetzungen
* JDK 11 oder 12;
* Gradle;

## Starten
* Im Verzeichnis der Applikation `./gradlew bootRun` (auf Unix-Systemen) oder `./gradlew.bat bootRun` (auf Windows-Systemen) ausführen;
* Im Browser auf http://localhost:8081/index.html gehen;
* einen neuen Benutzer erstellen;
* mit dem neuen Benutzer anmelden;
* Benutzer und Einträge verwalten;
* viel Spass!
* in der Date dump.sql findet sich ein Skript zum Einfügen von Beispieldaten

## Was noch zu tun ist
* Ändern ist noch nirgends möglich
* Zur Zeit haben alle Benutzer die Möglichkeit, andere Benutzer zu verwalten...

### Dokumentation
* Dokumentation: [DokumentationAufgabeB.pdf](DokumentationAufgabeB.pdf)
* Versionsgeschichte:  
![Hier sollte ein Bild zu sehen sein](versionsgeschichte.jpg)
