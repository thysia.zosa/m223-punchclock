const URL = 'http://localhost:8081';
const authHeader = localStorage.getItem('auth');
const username = localStorage.getItem('username');
let users = [];

const createUser = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const userData = {};
    userData['username'] = formData.get('username');
    userData['password'] = formData.get('password');
    if (userData['username'] === "" || userData['password'] === "") {
        return alert('Alle Felder müssen ausgefüllt werden');
    }
    if (userData['password'] !== formData.get('repeatPassword')) {
        return alert('Das Passwort stimmt nicht überein.');
    }

    fetch(`${URL}/users/sign-up`, {
        method: 'POST',
        headers: {
            'Authorization': authHeader,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    }).then((result) => {
        result.json().then((userData) => {
            users.push(userData);
            renderUsers();
        });
    });
};

const changeUser = (user) => {}


const deleteUser = (userId) => {
    users = [];
    fetch(`${URL}/users/${userId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        if (result.status !== 204) {
            alert('Ein Problem beim Löschen ist aufgetreten.');
        }
        indexUsers();
    });
};

const indexUsers = () => {
    fetch(`${URL}/users`, {
        method: 'GET',
        headers: {
            'Authorization': authHeader,
        }
    }).then((result) => {
        result.json().then((result) => {
            users = result;
            renderUsers();
        });
    });
    renderUsers();
};

const createCell = (text) => {
    const cell = document.createElement('td');
    cell.innerText = text;
    return cell;
};

const createButton = (entryId, text, buttonFunction) => {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = text;
    button.onclick = function () {
        console.log(entryId);
        buttonFunction(entryId);
    };
    cell.appendChild(button);
    return cell;
};

const renderUsers = () => {
    const display = document.querySelector('#userDisplay');
    display.innerHTML = '';
    users.forEach((user) => {
        const row = document.createElement('tr');
        row.appendChild(createCell(user.id));
        row.appendChild(createCell(user.username));
        row.appendChild(createButton(user.id, 'Löschen', deleteUser));
        row.appendChild(createButton(user, 'Ändern', changeUser));
        display.appendChild(row);
    });
};

document.addEventListener('DOMContentLoaded', function(){
    const createUserForm = document.querySelector('#createUserForm');
    createUserForm.addEventListener('submit', createUser);
    indexUsers();
});