const URL = 'http://localhost:8081';
let isLoginForm = true;

const switchForm = () => {
    let submitButton = document.getElementById('submitButton');
    let switchButton = document.getElementById('switchButton');
    let repPass = document.getElementById('repeatPassword');
    if (isLoginForm) {
        isLoginForm = false;
        submitButton.value = 'Neues Konto erstellen';
        switchButton.innerText = 'Schon registriert?';
        repPass.style.display = 'block'
    } else {
        isLoginForm = true;
        submitButton.value = 'Anmelden';
        switchButton.innerText = 'Noch kein Benutzerkonto?';
        repPass.style.display = 'none';
    }
};

const loginRegisterUser = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const userData = {};
    userData['username'] = formData.get('username');
    userData['password'] = formData.get('password');
    if (userData['username'] === "" || userData['password'] === "") {
        return alert('Alle Felder müssen ausgefüllt werden');
    }

    if (isLoginForm) {
        fetch(`${URL}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        }).then((result) => {
            if (result.status !== 200) {
                return alert('Benutzername oder Passwort nicht korrekt.')
            }
            let authHeader = result.headers.get('Authorization');
            localStorage.setItem('auth', authHeader);
            localStorage.setItem('username', userData['username']);
        });
        window.location.href = `${URL}/entries.html`;

    } else {
        if (userData['password'] !== formData.get('repeatPassword')) {
            return alert('Das Passwort stimmt nicht überein.');
        }
        fetch(`${URL}/users/sign-up`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        }).then((result) => {
            if (result.status !== 202) {
                alert('Der Benutzer konnte nicht erstellt werden.');
            } else {
                switchForm();
                alert('Der Benutzer wurde erfolgreich erstellt.')
            }
        });

    }

};



document.addEventListener('DOMContentLoaded', function () {
    const loginRegisterForm = document.getElementById('loginregisterForm');
    loginRegisterForm.addEventListener('submit', loginRegisterUser);
});