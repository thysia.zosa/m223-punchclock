package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.ApplicationUser;
import ch.zli.m223.punchclock.domain.Entry;
import ch.zli.m223.punchclock.repository.EntryRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EntryService {
	private EntryRepository entryRepository;

	public EntryService(EntryRepository entryRepository) {
		this.entryRepository = entryRepository;
	}

	public Entry createEntry(Entry entry) {
		return entryRepository.saveAndFlush(entry);
	}

	public List<Entry> findAll() {
		return entryRepository.findAll();
	}

	public List<Entry> findAllMine(ApplicationUser user) {
		return entryRepository.findAllByUser(user, Sort.unsorted());
	}

	public Entry updateEntry(Entry entry) {
		return entryRepository.saveAndFlush(entry);
	}

	public void deleteEntry(Long id) {
		try {
			entryRepository.deleteById(id);
		} catch (Exception e) {
			System.out.println("error");
		}
	}
}
