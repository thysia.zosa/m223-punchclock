# Aufgabe B - Vorüberlegungen

## Mindestanforderungen

### Der Akteur muss sich über eine graphische Benutzeroberfläche am System registrieren und anmelden können.
* Sign-up und login sind im Backend bereits implementiert. Das Frontend ist noch zu erweitern (für morgen; heute ist nur die Planung zu erledigen).

### Der Akteur muss Benutzer verwalten (erstellen, bearbeiten und löschen) können.
* Der Administrator kann Benutzer erstellen, bearbeiten und löschen. Dies ist ein neues Feature.

### Der Akteur muss eine weitere Entität verwalten (erstellen, bearbeiten und löschen) können.
Jeder Benutzer kann Einträge verwalten. Dieses Feature besteht bereits, ausgenommen die Verbindung mit dem Login.

### Es gibt einen Authentifizierungsmechanismus.
* Mit dem JWToken eingearbeitet. 

### Es gibt eine Schnittstelle, über die die Applikation benutzt werden kann.
* Mit dem Bisherigen vorgegeben.
